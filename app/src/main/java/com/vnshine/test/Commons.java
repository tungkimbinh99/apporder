package com.vnshine.test;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Commons {

    public static String regexData(String partern, String dataRegex, String replace) {
        Matcher matcherItem = Pattern.compile(partern).matcher(dataRegex);
        String dataSuccess = "";
        while (matcherItem.find()) {
            if (!TextUtils.isEmpty(matcherItem.group())) {
                return matcherItem.group().replace(replace, "");
            }
        }
        return dataSuccess;
    }
}
