package com.vnshine.test;


public class ItemDetail{
    private String image_model;
    private String image_origin;
    private String item_id;
    private String link_origin;
    private String price_origin;
    private String price_promotion;
    private String property;
    private String quantity;
    private String shop_id;
    private String shop_name;
    private String site;
    private String stock = "0";
    private String title_origin;
    private String wangwang;

    public void setImage_model(String image_mode) {
        this.image_model = image_mode;
    }

    public void setTitle_origin(String title_origin) {
        this.title_origin = title_origin;
    }

    public void setImage_origin(String image_origin) {
        this.image_origin = image_origin;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public void setLink_origin(String link_origin) {
        this.link_origin = link_origin;
    }

    public void setPrice_origin(String price_origin) {
        this.price_origin = price_origin;
    }

    public void setPrice_promotion(String price_promotion) {
        this.price_promotion = price_promotion;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public void setWangwang(String wangwang) {
        this.wangwang = wangwang;
    }

    public String getImage_model() {
        return this.image_model;
    }

    public String getTitle_origin() {
        return this.title_origin;
    }

    public String getImage_origin() {
        return this.image_origin;
    }

    public String getItem_id() {
        return this.item_id;
    }

    public String getLink_origin() {
        return this.link_origin;
    }

    public String getPrice_origin() {
        return this.price_origin;
    }

    public String getPrice_promotion() {
        return this.price_promotion;
    }

    public String getProperty() {
        return this.property;
    }

    public String getQuantity() {
        return this.quantity;
    }

    public String getShop_id() {
        return this.shop_id;
    }

    public String getShop_name() {
        return this.shop_name;
    }

    public String getSite() {
        return this.site;
    }

    public String getStock() {
        return this.stock;
    }

    public String getWangwang() {
        return this.wangwang;
    }
}
