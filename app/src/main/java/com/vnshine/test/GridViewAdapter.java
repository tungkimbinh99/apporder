package com.vnshine.test;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

public class GridViewAdapter extends BaseAdapter {
    List<the> mStringList;


    public GridViewAdapter(List<the> mStringList) {
        this.mStringList = mStringList;
    }

    @Override
    public int getCount() {
        return mStringList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    class ViewHolder{
        RadioButton button;
        TextView textView;
    }
    public void check(int i){
        mStringList.get(i).setIscheck(true);
        notifyDataSetChanged();
    }
    public void reset(){
        for (int k =0;k<mStringList.size();k++){
                mStringList.get(k).setIscheck(false);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder ;
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView== null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_gridlview,null);
            holder.button = convertView.findViewById(R.id.radio);
            holder.textView = convertView.findViewById(R.id.text);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        final the e = mStringList.get(position);
        if (e.isIscheck()){
            holder.button.setChecked(true);
        }else {
            holder.button.setChecked(false);
        }
//        holder.button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (e.isIscheck()){
//                    e.setIscheck(false);
//                }else {
//                    e.setIscheck(true);
//                }
//                check(position);
//                notifyDataSetChanged();
//            }
//        });
        holder.textView.setText(mStringList.get(position).getName());
        return convertView;
    }
}
