package com.vnshine.test;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.security.NetworkSecurityPolicy;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.StringTokenizer;


public class WebviewActivity extends AppCompatActivity {

        private static final String LINK = "LINK";
        WebView mWebView;
        String originUrl;
        TextView titleBarTxt;
        ImageView backBtn;
        Button btnaddcart;
        RelativeLayout groupcart;
        int dem=0;
        private AsyncTask<Void, Void, String> addJs;
        @SuppressLint({"JavascriptInterface", "WrongConstant"})
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
//        SplashActivity.setLanguage(this);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted();
            }
            setContentView(R.layout.activity_webview);
            btnaddcart= findViewById(R.id.addcart);
            groupcart= findViewById(R.id.groupcart);
            mWebView=findViewById(R.id.webview);
            groupcart.setVisibility(View.GONE);

            btnaddcart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (WebviewActivity.this.originUrl.contains("https://m.1688.com/offer/")) {
                        mWebView.loadUrl("javascript:getDetailsInfomation1688()");
                    }else {
                        mWebView.loadUrl("javascript:getDetailsInfomation()");
                    }

                }
            });
//            backBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    onBackPressed();
////                if(mWebView.canGoBack()){
////                    mWebView.goBack();
////                }else
////                    finish();
//                }
//            });



//            this.originUrl = "https://m.1688.com/offer/596793796684.html?offerId=596793796684&scm=1007.16274.101999.wl_scm_gul_sx_0&pvid=e8a7cb73-aa53-406e-bd35-40a098220864&bizid=hot&spm=a26g8.m1688.m-you-may-like.3";

this.originUrl = "https://www.tmall.com/";
            Log.e("TAG","Het Oncreate: "+originUrl);
            configWebview();
        }

        @Override
        public void onBackPressed() {
            if(mWebView.canGoBack()){
                mWebView.goBack();
            }else
                finish();

        }

        @SuppressLint({"SetJavaScriptEnabled", "ObsoleteSdkInt"})
        private void configWebview() {
            this.mWebView.getSettings().setJavaScriptEnabled(true);
            if (Build.VERSION.SDK_INT >= 16) {
                this.mWebView.getSettings().setAllowFileAccessFromFileURLs(true);
                this.mWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);
            }
            this.mWebView.addJavascriptInterface(new MyJavaScriptInterface(), "Android");
            this.mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            this.mWebView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 5.1.1; Nexus 5 Build/LMY48B; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/43.0.2357.65 Mobile Safari/537.36");
            if (Build.VERSION.SDK_INT >= 11) {
                this.mWebView.getSettings().setDisplayZoomControls(false);
            }
//        this.mWebView.getSettings().setCacheMode(2);
            if (Build.VERSION.SDK_INT >= 19) {
                this.mWebView.setLayerType(2, null);
            } else {
                this.mWebView.setLayerType(1, null);
            }
            this.mWebView.getSettings().setAppCacheEnabled(false);
            this.mWebView.getSettings().setDomStorageEnabled(true);
            this.mWebView.getSettings().setLoadWithOverviewMode(true);
            this.mWebView.getSettings().setUseWideViewPort(true);
            this.mWebView.setWebChromeClient(new MyWebChromeClient());
            this.mWebView.setWebViewClient(new MyWebViewClient(this.originUrl));
            if (Build.VERSION.SDK_INT >= 21) {
                CookieManager cookieManager = CookieManager.getInstance();
                cookieManager.setAcceptThirdPartyCookies(this.mWebView, false);
                cookieManager.setCookie(this.originUrl, "");
            }
            this.mWebView.loadUrl(this.originUrl);
        }
        

        private class MyWebViewClient extends WebViewClient {

            private String addressBar;

            public MyWebViewClient(String addressBar) {
                this.addressBar= addressBar;
            }


            // Khi bạn click vào link bên trong trình duyệt (Webview)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i("MyLog","Click on any interlink on webview that time you got url :-" + url);
//            addressBar.setText(url);
            WebviewActivity.this.originUrl=url;
                return super.shouldOverrideUrlLoading(view, url);
            }


            // Khi trang bắt đầu được tải
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                WebviewActivity.this.originUrl=url;
                Log.e("TAG","Loading trang: : "+originUrl);
                if (WebviewActivity.this.originUrl.contains("https://m.1688.com/offer/") && WebviewActivity.this.originUrl.contains("spm")) {
                    Log.e("TAG", "oke 1688" );
                    groupcart.setVisibility(View.VISIBLE);
                } else if (WebviewActivity.this.originUrl.contains("taobao.com/detail/detail.html")) {
                    groupcart.setVisibility(View.VISIBLE);
                } else if (WebviewActivity.this.originUrl.contains("detail.m.tmall.com/item.htm")) {
                    groupcart.setVisibility(View.VISIBLE);
                } else if (WebviewActivity.this.originUrl.contains("https://s.m.taobao.com/h5?q")) {
                    groupcart.setVisibility(View.VISIBLE);
                } else if (WebviewActivity.this.originUrl.contains("https://list.tmall.com/search_product.htm?q")) {
                    groupcart.setVisibility(View.GONE);
                } else if (WebviewActivity.this.originUrl.contains("https://m.1688.com/offer/")) {
                    groupcart.setVisibility(View.VISIBLE);
                }else {
                    groupcart.setVisibility(View.GONE);
                }
                Log.i("MyLog", "Your current url when webpage loading.." + url);
            }


            // Khi trang tải xong
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onPageFinished(final WebView view, String url) {
                Log.i("MyLog", "Your current url when webpage loading.. finish" + url);
                super.onPageFinished(view, url);
                WebviewActivity.this.addJs = new AsyncTask<Void, Void, String>() {
                    protected String doInBackground(Void... voids) {
                        String encoded = "";
                        try {
                            InputStream input = WebviewActivity.this.getAssets().open("getdata.js");
                            byte[] buffer = new byte[input.available()];
                            input.read(buffer);
                            input.close();
                            encoded = Base64.encodeToString(buffer, 2);
                            return encoded;
                        } catch (IOException e) {
                            e.printStackTrace();
                            return "";
                        } catch (NullPointerException e2) {
                            return "";
                        }
                    }

                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        if (!TextUtils.isEmpty(s)) {
                            view.loadUrl("javascript:(function() {var parent = document.getElementsByTagName('head').item(0);var script = document.createElement('script');script.type = 'text/javascript';script.innerHTML = window.atob('" + s + "');parent.appendChild(script)})()");
                        }
                        if (view == null) {
                            return;
                        }
                        if (WebviewActivity.this.originUrl.contains("https://m.1688.com/offer/") && WebviewActivity.this.originUrl.contains("spm")) {
                            view.loadUrl("javascript:changeData1688()");
                            Log.e("TAG", "change 1688 " );

                        } else if (WebviewActivity.this.originUrl.contains("taobao.com/detail/detail.html")) {
                            view.loadUrl("javascript:changeDataTaobao()");

                        } else if (WebviewActivity.this.originUrl.contains("detail.m.tmall.com/item.htm")) {
                            view.loadUrl("javascript:changeDataTaobao()");

                        } else if (WebviewActivity.this.originUrl.contains("https://s.m.taobao.com/h5?q")) {
                            view.loadUrl("javascript:removeElement()");

                        } else if (WebviewActivity.this.originUrl.contains("https://list.tmall.com/search_product.htm?q")) {
                            view.loadUrl("javascript:removeElementTmall()");

                        } else if (WebviewActivity.this.originUrl.contains("https://m.1688.com/offer/")) {
                            view.loadUrl("javascript:removeElement1688()");

                        }
                    }
                };
                WebviewActivity.this.addJs.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
            }


        }

        class MyJavaScriptInterface
        {
            MyJavaScriptInterface() {}


            @JavascriptInterface
            public void getDataSuccess(String paramString)
            {
                Log.e("TAG", "data = " + paramString);
                if (WebviewActivity.this.originUrl.contains("https://m.1688.com/offer/")) {
                    parseData1688(paramString);
                }else {
                    parseDataTaobaoTmall(paramString);
                }


            }

            @JavascriptInterface
            public void timeout()
            {
                Log.e("TAG", "timeout");
            }
        }

        public class MyWebChromeClient
                extends WebChromeClient
        {
            MyWebChromeClient() {}

            public boolean onJsAlert(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult)
            {
                return true;
            }

            public boolean onJsConfirm(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult)
            {
                return true;
            }

            public void onPermissionRequest(PermissionRequest paramPermissionRequest)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    paramPermissionRequest.grant(paramPermissionRequest.getResources());
                }
            }

            public void onProgressChanged(WebView paramWebView, int paramInt) {}

            public void onReceivedTitle(WebView paramWebView, String paramString) {}

            public boolean onShowFileChooser(WebView paramWebView, ValueCallback paramValueCallback, FileChooserParams paramFileChooserParams)
            {
                return true;
            }

            public void openFileChooser(ValueCallback paramValueCallback) {}

            public void openFileChooser(ValueCallback paramValueCallback, String paramString) {}
        }

        private void parseDataTaobaoTmall(String data) {
            Log.e("TAG", "parseDataTaobaoTmall: " );
            try {
                String shop_title_wrapper = new JSONObject(data).getString("shop_title_wrapper");
                String shop_id = Commons.regexData("user\\_id\\=([^\\&]+)", shop_title_wrapper, "user_id=");
                String item_id = Commons.regexData("item\\_id\\=([0-9]+)", shop_title_wrapper, "item_id=");
                ItemDetail itemDetails = (ItemDetail) new Gson().fromJson(data, ItemDetail.class);
                if (itemDetails.getProperty().equals("1")) {
                    Toast.makeText(this, "Bạn vui lòng chọn đầy đủ màu sắc kích cỡ.", Toast.LENGTH_SHORT).show();
                    return;
                }
                itemDetails.setShop_id(shop_id);

                itemDetails.setItem_id(item_id);
                itemDetails.setLink_origin(this.originUrl);
                if (this.originUrl.contains("taobao")) {
                    itemDetails.setSite("taobao");
                } else if (this.originUrl.contains("tmall")) {
                    itemDetails.setSite("tmall");
                } else if (this.originUrl.contains("1688")) {
                    itemDetails.setSite("1688");
                }

                addDonHang(itemDetails);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        private void parseData1688(String data) {
            String shop_id="";
            String property="";
            try {
                String shop_title_wrapper = new JSONObject(data).getString("shop_id");
                property = new JSONObject(data).getString("property");
                int i=0;
                StringTokenizer tokenizer=  new StringTokenizer(shop_title_wrapper,",:" );
                StringBuffer kq= new StringBuffer();
                while (tokenizer.hasMoreTokens()){
                    String s=tokenizer.nextToken();
                    i++;
                    if(i==2)
                        Log.e("TAG", "item    "+s );
                    if (i==4){
                        kq.append(s);
                        kq.deleteCharAt(0);
                        kq.deleteCharAt(kq.length()-1);
                        shop_id=kq.toString();
                    }
                }
                StringBuilder kq1= new StringBuilder();
                kq1.append(property);
                kq1.delete(0,3);
                property=kq1.toString();
                Log.e("TAG", "parseData1688: "+property );



            } catch (JSONException e) {
                e.printStackTrace();
            }


            ItemDetail itemDetails = (ItemDetail) new Gson().fromJson(data, ItemDetail.class);
            if (itemDetails.getQuantity().contains("0")) {
                Toast.makeText(getApplicationContext(), "Bạn vui lòng chọn đầy đủ màu sắc kích cỡ.", Toast.LENGTH_SHORT).show();
                return;
            }


            itemDetails.setShop_id(shop_id);

            itemDetails.setLink_origin(this.originUrl);

            itemDetails.setSite("1688");

            addDonHang(itemDetails);
        }


        private void addDonHang(ItemDetail itemDetails) {
            Log.e("TAG","ADD DON");
          
        }

        @Override
        protected void onResume() {
            super.onResume();
        }
    

}
