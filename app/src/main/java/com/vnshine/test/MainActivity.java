package com.vnshine.test;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import static android.content.ContentValues.TAG;

public class MainActivity extends Activity {



    private AsyncTask<Void, Void, String> addJs;
    private WebView mWebView;
    private Button buttonGo;
    private Button buttonthu;
    private Button buttonweb;
    private Button buttonStatic;
    public String originUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonthu =(Button)findViewById(R.id.test);

        buttonGo =(Button)findViewById(R.id.button_go);
        buttonweb =(Button)findViewById(R.id.button_web);
        buttonStatic =(Button)findViewById(R.id.button_static);
        buttonweb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,WebviewActivity.class));
            }
        });
        buttonthu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,Main2Activity.class));
            }
        });
//
        mWebView =(WebView)findViewById(R.id.webView);
//        this.originUrl = "https://m.intl.taobao.com/detail/detail.html?spm=a220m.1000858.1000725.6.38dde9c6a9YM05&id=564517521771&skuId=3651580932170&user_id=2431870063&cat_id=2&is_b=1&rn=8378c1d6f4db26c8f744a1e0c2cc2f4b";
//        // Tùy biến WebViewClient để điều khiển các sự kiện trên WebView
//        webView.setWebViewClient(new MyWebViewClient(this.originUrl));

        buttonGo.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                goUrl();
            }
        });

        buttonStatic.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                mWebView.loadUrl("javascript:getDetailsInfomation()");
            }
        });
        this.originUrl = "https://m.intl.taobao.com/detail/detail.html?spm=a220m.1000858.1000725.6.38dde9c6a9YM05&id=564517521771&skuId=3651580932170&user_id=2431870063&cat_id=2&is_b=1&rn=8378c1d6f4db26c8f744a1e0c2cc2f4b";

//        this.originUrl = "https://m.1688.com/offer/596793796684.html?offerId=596793796684&scm=1007.16274.101999.wl_scm_gul_sx_0&pvid=e8a7cb73-aa53-406e-bd35-40a098220864&bizid=hot&spm=a26g8.m1688.m-you-may-like.3";
        configWebview();
    }

    private void goUrl()  {
        String url = this.originUrl;
        if(url.isEmpty())  {
            Toast.makeText(this,"Please enter url",Toast.LENGTH_SHORT).show();
            return;
        }

//        if (Build.VERSION.SDK_INT >= 16)
//        {
//            webView.getSettings().setAllowFileAccessFromFileURLs(true);
//            webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
//        }
//        webView.getSettings().setLoadsImagesAutomatically(true);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.addJavascriptInterface(new MyJavaScriptInterface(), "Android");
//        webView.setWebChromeClient(new MyWebChromeClient());
//        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//
//        webView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 5.1.1; Nexus 5 Build/LMY48B; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/43.0.2357.65 Mobile Safari/537.36");
//        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//        webView.loadUrl(url);
    }

    private void configWebview() {
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= 16) {
            this.mWebView.getSettings().setAllowFileAccessFromFileURLs(true);
            this.mWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        }
        this.mWebView.addJavascriptInterface(new MyJavaScriptInterface(), "Android");
        this.mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        this.mWebView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 5.1.1; Nexus 5 Build/LMY48B; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/43.0.2357.65 Mobile Safari/537.36");
        if (Build.VERSION.SDK_INT >= 11) {
            this.mWebView.getSettings().setDisplayZoomControls(false);
        }
//        this.mWebView.getSettings().setCacheMode(2);
        if (Build.VERSION.SDK_INT >= 19) {
            this.mWebView.setLayerType(2, null);
        } else {
            this.mWebView.setLayerType(1, null);
        }
        this.mWebView.getSettings().setAppCacheEnabled(false);
        this.mWebView.getSettings().setDomStorageEnabled(true);
        this.mWebView.getSettings().setLoadWithOverviewMode(true);
        this.mWebView.getSettings().setUseWideViewPort(true);
        this.mWebView.setWebChromeClient(new MyWebChromeClient());
        this.mWebView.setWebViewClient(new MyWebViewClient(this.originUrl));
//        if (TextUtils.isEmpty(this.originUrl)) {
//            if (this.mIndexSelectedSpinner == 0) {
//                this.originUrl = "https://s.m.taobao.com/h5?q=" + this.keySearch;
//            } else if (this.mIndexSelectedSpinner == 1) {
//                this.originUrl = "https://list.tmall.com/search_product.htm?q=" + this.keySearch;
//            } else {
//                this.originUrl = "http://m.1688.com/page/search.html?type=offer&keywords=" + this.keySearch;
//            }
//            Log.e("linksearch", "linksearch = " + this.originUrl);
//        }
        if (Build.VERSION.SDK_INT >= 21) {
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptThirdPartyCookies(this.mWebView, false);
            cookieManager.setCookie(this.originUrl, "");
        }
        this.mWebView.loadUrl(this.originUrl);
    }

//    private void showStaticContent()  {
//        String staticContent="<h2>Select web page</h2>"
//                + "<ul><li><a href='http://eclipse.org'>Eclipse</a></li>"
//                +"<li><a href='http://google.com'>Google</a></li></ul>";
//        webView.loadData(staticContent, "text/html", "UTF-8");
//    }

    private class MyWebViewClient extends WebViewClient {

        private String addressBar;

        public MyWebViewClient(String addressBar) {
            this.addressBar= addressBar;
        }


        // Khi bạn click vào link bên trong trình duyệt (Webview)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.i("MyLog","Click on any interlink on webview that time you got url :-" + url);
//            addressBar.setText(url);
            return super.shouldOverrideUrlLoading(view, url);
        }


        // Khi trang bắt đầu được tải
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.i("MyLog", "Your current url when webpage loading.." + url);
        }


        // Khi trang tải xong
        @Override
        public void onPageFinished(final WebView view, String url) {
            Log.i("MyLog", "Your current url when webpage loading.. finish" + url);
            super.onPageFinished(view, url);
            MainActivity.this.addJs = new AsyncTask<Void, Void, String>() {
                protected String doInBackground(Void... voids) {
                    String encoded = "";
                    try {
                        if (MainActivity.this != null) {
                            InputStream input = MainActivity.this.getAssets().open("getdata.js");
                            byte[] buffer = new byte[input.available()];
                            input.read(buffer);
                            input.close();
                            encoded = Base64.encodeToString(buffer, 2);
                        }
                        return encoded;
                    } catch (IOException e) {
                        e.printStackTrace();
                        return "";
                    } catch (NullPointerException e2) {
                        return "";
                    }
                }

                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    if (!TextUtils.isEmpty(s)) {
                        view.loadUrl("javascript:(function() {var parent = document.getElementsByTagName('head').item(0);var script = document.createElement('script');script.type = 'text/javascript';script.innerHTML = window.atob('" + s + "');parent.appendChild(script)})()");
                    }
                    if (view == null) {
                        return;
                    }
                    if (MainActivity.this.originUrl.contains("http://m.1688.com/offer/") && MainActivity.this.originUrl.contains("spm")) {
                        view.loadUrl("javascript:changeData1688()");
                    } else if (MainActivity.this.originUrl.contains("taobao.com/detail/detail.html")) {
                        view.loadUrl("javascript:changeDataTaobao()");
                    } else if (MainActivity.this.originUrl.contains("detail.m.tmall.com/item.htm")) {
                        view.loadUrl("javascript:changeDataTaobao()");
                    } else if (MainActivity.this.originUrl.contains("https://s.m.taobao.com/h5?q")) {
                        view.loadUrl("javascript:removeElement()");
                    } else if (MainActivity.this.originUrl.contains("https://list.tmall.com/search_product.htm?q")) {
                        view.loadUrl("javascript:removeElementTmall()");
                    } else if (MainActivity.this.originUrl.contains("https://m.1688.com/offer/")) {
                        view.loadUrl("javascript:removeElement1688()");
                    }
                }
            };
            MainActivity.this.addJs.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }


    }

    class MyJavaScriptInterface
    {
        MyJavaScriptInterface() {}

        @JavascriptInterface
        public void getDataSuccess(String paramString)
        {
            Log.e("data", "data = " + paramString);

                parseDataTaobaoTmall(paramString);

        }

        @JavascriptInterface
        public void timeout()
        {
            Log.e(TAG, "timeout");
        }
    }

    public class MyWebChromeClient
            extends WebChromeClient
    {
        public MyWebChromeClient() {}

        public boolean onJsAlert(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult)
        {
            return true;
        }

        public boolean onJsConfirm(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult)
        {
            return true;
        }

        public void onPermissionRequest(PermissionRequest paramPermissionRequest)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                paramPermissionRequest.grant(paramPermissionRequest.getResources());
            }
        }

        public void onProgressChanged(WebView paramWebView, int paramInt) {}

        public void onReceivedTitle(WebView paramWebView, String paramString) {}

        public boolean onShowFileChooser(WebView paramWebView, ValueCallback paramValueCallback, WebChromeClient.FileChooserParams paramFileChooserParams)
        {
            return true;
        }

        public void openFileChooser(ValueCallback paramValueCallback) {}

        public void openFileChooser(ValueCallback paramValueCallback, String paramString) {}
    }

    @SuppressLint("WrongConstant")
    private void parseDataTaobaoTmall(String data) {
        try {
            String shop_title_wrapper = new JSONObject(data).getString("shop_title_wrapper");
            String shop_id = Commons.regexData("user\\_id\\=([^\\&]+)", shop_title_wrapper, "user_id=");
            String item_id = Commons.regexData("item\\_id\\=([0-9]+)", shop_title_wrapper, "item_id=");
            ItemDetail itemDetails = (ItemDetail) new Gson().fromJson(data, ItemDetail.class);
            if (itemDetails.getProperty().equals("1")) {
                Toast.makeText(this, "Bạn vui lòng chọn đầy đủ màu sắc kích cỡ.", 0).show();
                return;
            }
            itemDetails.setShop_id(shop_id);
            itemDetails.setItem_id(item_id);
            itemDetails.setLink_origin(this.originUrl);
            if (this.originUrl.contains("taobao")) {
                itemDetails.setSite("taobao");
            } else if (this.originUrl.contains("tmall")) {
                itemDetails.setSite("tmall");
            } else if (this.originUrl.contains("1688")) {
                itemDetails.setSite("1688");
            }
            addDonHang(itemDetails);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addDonHang(ItemDetail itemDetails) {

    }

}