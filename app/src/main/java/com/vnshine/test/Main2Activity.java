package com.vnshine.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity {
    GridView gridView;
    GridViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        gridView = this.findViewById(R.id.gridview);
        List<the> mlist = new ArrayList<>();
        for (int i=1;i<=9;i++){
            the e = new the();
            e.setName("the"+i);
            mlist.add(e);
        }
        adapter = new GridViewAdapter(mlist);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.reset();
                adapter.check(position);
                Toast.makeText(Main2Activity.this, "the "+position, Toast.LENGTH_SHORT).show();
            }
        });

    }
}
